import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main(){
  runApp(const NeoApp());//Application
}

class NeoApp extends StatelessWidget{
  const NeoApp({super.key});

  @override
  Widget build(BuildContext context) {

    return  MaterialApp(
      theme: ThemeData(primarySwatch: Colors.deepPurple),
        darkTheme: ThemeData(primarySwatch: Colors.deepPurple),
        color: Colors.blue,
        debugShowCheckedModeBanner: false,
        home: HomeActivity()

    );

  }

}

class HomeActivity extends StatelessWidget{
  const HomeActivity({super.key});

MySnackBar(message,context)
{
  return ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(content: Text(message))
  );

}
  MyAlertDialog(context){
    return showDialog(
        context: context,


        builder:(BuildContext context){
          return Expanded(
              child: AlertDialog(
                title: Text("Alert !"),
                content: Text("Delete Or Not"),
                actions: [
                 TextButton(onPressed: (){
                   MySnackBar("Deletion Success", context);
                   Navigator.of(context).pop();
                 }, child: Text("Yes")),
                 TextButton(onPressed: (){Navigator.of(context).pop();}, child: Text("No")),

                ],

              )


          );

        }

    );


  }


  @override
  Widget build(BuildContext context) {


     return Scaffold(
       appBar: AppBar(
         title: Text("Neon Application"),
       ),

     body: Center(
         child: ElevatedButton(child: Text("Click"),onPressed:(){MyAlertDialog(context);} ,),



     )



     );

  }





}
