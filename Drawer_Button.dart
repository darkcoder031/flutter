@override
  Widget build(BuildContext context) {
     return Scaffold(
       appBar: AppBar(
         title: Text("Neon Application"),
         titleSpacing: 4,
         //centerTitle: true,
         toolbarHeight: 50,
         toolbarOpacity: 1,
         elevation: 0,
         backgroundColor: Colors.blue,
         actions: [
           IconButton(onPressed:(){MySnackBar("Hey Comments", context);}, icon: Icon(Icons.comment)),
           IconButton(onPressed:(){MySnackBar("Search Yourself", context);}, icon: Icon(Icons.search)),
           IconButton(onPressed:(){MySnackBar("Settings", context);}, icon: Icon(Icons.settings)),

         ],

       ),
      floatingActionButton: FloatingActionButton(

        elevation: 10,
        child: Icon(Icons.add),
        backgroundColor: Colors.green ,
        onPressed: (){
        MySnackBar("Hey floating Action Button", context);

        },


      ),
      bottomNavigationBar: BottomNavigationBar(

        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home),label: "Home"),
          BottomNavigationBarItem(icon: Icon(Icons.contact_mail),label: "Contact"),
          BottomNavigationBarItem(icon: Icon(Icons.verified_user),label: "Profile"),

         ],
     onTap: (int index){
          if(index==0)
          {
            MySnackBar("Home Button", context);
          }
          if(index==1)
            {
              MySnackBar("Contact Button", context);
            }
          if(index==2)
            {
              MySnackBar("User Profile", context);
            }

     },


      ),

      drawer: Drawer(

          child: ListView(

             children: [
               UserAccountsDrawerHeader(accountName:Text("NAHID HASAN"), 
                   accountEmail:Text("neonhasan535@gmail.com"),
                   currentAccountPicture: Icon(Icons.verified_user),
               
               
               
               ),
               ListTile(
                 leading:Icon(Icons.home) ,
                 title: Text("Home"),tileColor:Colors.green,
               onTap: (){
                   MySnackBar("Home", context);
               },

               ),
               ListTile(leading:Icon(Icons.email) ,title: Text("Email"),tileColor:Colors.green,),
               ListTile(leading:Icon(Icons.contact_phone) ,title: Text("Contact"),tileColor:Colors.green,),
               ListTile(leading:Icon(Icons.verified_user_rounded) ,title: Text("User"),tileColor:Colors.green,),

             ],

          ),



      ),
       body: Text("Hello Nahid"),



     );//Scaffold
