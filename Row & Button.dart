
  @override
  Widget build(BuildContext context) {

  ButtonStyle buttonStyle = ElevatedButton.styleFrom(
   padding: EdgeInsets.all(25),
    backgroundColor: Colors.pink,
    foregroundColor: Colors.white,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(20))

    )


  );




     return Scaffold(
       appBar: AppBar(
         title: Text("Neon Application"),
         titleSpacing: 4,
         //centerTitle: true,
         toolbarHeight: 50,
         toolbarOpacity: 1,
         elevation: 0,
         backgroundColor: Colors.blue,
         actions: [
           IconButton(onPressed:(){MySnackBar("Hey Comments", context);}, icon: Icon(Icons.comment)),
           IconButton(onPressed:(){MySnackBar("Search Yourself", context);}, icon: Icon(Icons.search)),
           IconButton(onPressed:(){MySnackBar("Settings", context);}, icon: Icon(Icons.settings)),

         ],

       ),

       body: Row(
         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children :[
          TextButton(onPressed: (){}, child: Text("Click")),
          ElevatedButton(onPressed: (){}, child: Text("Click"),style: buttonStyle,),
          IconButton.outlined(onPressed:(){}, icon:Icon(Icons.add)),
          OutlinedButton(onPressed: (){}, child: Text("Hello"))

        ],




       ) ,


     );

  }
