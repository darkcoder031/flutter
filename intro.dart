//structure of dart project
// Main --> My App --> Material App--> Activity page --> Scaffold

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main(){
  runApp(const NeoApp());//Application
}

class NeoApp extends StatelessWidget{
  const NeoApp({super.key});

  @override
  Widget build(BuildContext context) {

    return  MaterialApp(
      theme: ThemeData(primarySwatch: Colors.deepPurple),
        darkTheme: ThemeData(primarySwatch: Colors.deepPurple),
        color: Colors.blue,
        debugShowCheckedModeBanner: false,
        home: HomeActivity()

    );


  }

}

class HomeActivity extends StatelessWidget{
  const HomeActivity({super.key});

MySnackBar(message,context)
{
  return ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(content: Text(message))
  );


}

  @override
  Widget build(BuildContext context) {
     return Scaffold(
    //app Bar
       appBar: AppBar(
         title: Text("Neon Application"),
         titleSpacing: 4,
         //centerTitle: true,
         toolbarHeight: 50,
         toolbarOpacity: 1,
         elevation: 0,
         backgroundColor: Colors.blue,
         actions: [
           IconButton(onPressed:(){MySnackBar("Hey Comments", context);}, icon: Icon(Icons.comment)),
         ],

       ),
       //Body
       body: Text("Hello World"),


     );

  }





}
